TARGET_GLOBAL_CFLAGS += -mfpu=neon -mfloat-abi=softfp
TARGET_GLOBAL_CPPFLAGS += -mfpu=neon -mfloat-abi=softfp
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_ARCH := arm
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_CPU_VARIANT := krait

# Kernel 
BOARD_KERNEL_CMDLINE  := console=null androidboot.hardware=qcom user_debug=31 msm_rtb.filter=0x3F ehci-hcd.park=3 maxcpus=4 androidboot.selinux=permissive
BOARD_KERNEL_BASE     := 0x80200000
BOARD_KERNEL_PAGESIZE := 2048
BOARD_MKBOOTIMG_ARGS  := --ramdisk_offset 0x02000000
TARGET_PREBUILT_KERNEL := device/nubia/nx40x/kernel

# Bootloader
TARGET_BOOTLOADER_NAME       := nx40x
TARGET_BOOTLOADER_BOARD_NAME := MSM8960

TARGET_RECOVERY_PIXEL_FORMAT := "RGBX_8888"

# TWRP Recovery
TARGET_RECOVERY_FSTAB            := device/nubia/nx40x/recovery.fstab
DEVICE_RESOLUTION                := 720x1280
TW_INTERNAL_STORAGE_PATH         := "/sdcard"
TW_INTERNAL_STORAGE_MOUNT_POINT  := "sdcard"
TW_EXTERNAL_STORAGE_PATH         := "/external_sd"
TW_EXTERNAL_STORAGE_MOUNT_POINT  := "external_sd"
TW_NO_REBOOT_BOOTLOADER          := true
TW_HAS_DOWNLOAD_MODE             := false
TWRP_EVENT_LOGGING               := false
RECOVERY_GRAPHICS_USE_LINELENGTH := true
TW_NO_SCREEN_BLANK := true
TW_NO_SCREEN_TIMEOUT := true
TW_CUSTOM_BATTERY_PATH := /sys/class/power_supply/battery
TW_BRIGHTNESS_PATH := /sys/class/leds/lcd-backlight/brightness
TW_MAX_BRIGHTNESS := 191
# TWRP f2fs support
TARGET_USERIMAGES_USE_F2FS := true
# TWRP crypto support
TW_INCLUDE_JB_CRYPTO := true

# Assert
TARGET_OTA_ASSERT_DEVICE := nx40x,nx401,nx402,NX40X,NX401,NX402